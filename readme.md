# Who Can Help Me? #

Who Can Help Me? is a web application that allows communities to capture skill profiles, making it easier to find people who can help you. 

It is also a demonstration of the current way I currently build web applications that require an web-based user interface backed by a relational database.

## Why? ##

Who Can Help Me? was originally written in just over 3 hours using ASP.NET WebForms and LINQ to SQL to try and solve the business problem of creating a searchable skills matrix that would allow people within an organisation find other people who had specific skills or expertise who could help them solve a particular problem. The application was then used as a sample application for the Sharp Architecture framework, using ASP.NET MVC, NHibernate and many other Open Source Frameworks & Tools.

This latest iteration removes the dependency on S#arp Architecture and embraces a number of the features introduced in ASP.NET MVC 3 and 4. I built it as a reference point for myself to use when building similarly architected applications in the future.

## Well done me... or not. ##

I would be doing those friends and colleagues who I've been lucky enough to work with and learn from a huge disservice if I claimed credit for this. So here's some people I rate extremely highly:

- **Howard van Rooijen** (who spent the original 3 hours building WCHM mark 1 and is responsible for the extreme greeness) - [blog](http://blog.endjin.com/) - [twitter](https://twitter.com/howardvrooijen)
- **James Broome** - [twitter](https://twitter.com/broomej)

# Points of Interest #

This section aims to draw attention to specific sections of the codebase, and explain why things are done the way they are.

## Layers ##

The application is split into the following layers:

- Presentation - starting from the top, this layer contains the MVC app and is the main entry point for the solution. It contains controllers, views (using the Razor view engine), view models and form models and other supporting code.
- Domain - this layer contains the classes that represent (wait for it) our domain. In an ideal world, their names would have meaning to the business users you're working with. The layer also contains contracts for any domain or third party services the application makes use of - in our case, we have a third party news service.
- Application Services - This layer contains code that the presentation layer uses to interact with the domain layer. In the case of WCHM, this is done using a command based approach, detailed below.
- Infrastructure - This layer contains the wiring - the stuff that is carefully abstracted away from the presentation layer by the domain/app services layers is all pushed down here. In this layer we find the code that handles persistence, implements communications with third party services, and so on.

## Initialisation ##

If you open global.asax.cs, you'll notice the near-total absence of any configuration code. This is because the app is using the excellent WebActivator package ([details on GitHub](https://github.com/davidebbo/WebActivator) by [David Ebbo](http://blog.davidebbo.com/).

By convention, startup code is placed in static classes in the App_Start folder. Each static class has a static method that acts as the entry point, and each class file also contains an assembly-level PreApplicationStartMethod/PostApplicationStartMethod attribute that references this method. You can see examples in BundleConfig, FilterConfig, etc.

## Inversion of Control ##

Inversion of Control is implemented using the built in ASP.NET MVC Dependency Resolver with Castle Windsor under the covers. You will see initialisation is in three stages:

- Create a new instance of the Windsor Container, and add it to itself. This is often considered to be an antipattern, but there are a couple of points in the codebase where I consider this is a valid approach.
- Set up the ASP.NET MVC dependency resolver. There are a couple of ways of doing this; I'm using the approach of passing in lambdas to wrap the Windsor calls. Windsor will throw an exception if an attempt is made to resolve a component that has not been registered, but the MVC framework expects DependencyResolver to return null (for Resolve) or an empty array (for ResolveAll) - hence both the lambdas verify the component exists before attempting to resolve it.
- Install the services. This is done using Castle Windsor's [excellent Installers approach](http://docs.castleproject.org/Default.aspx?Page=Installers&NS=Windsor&AspxAutoDetectCookieSupport=1) to keep registration logic close to the components being registered. The code in IoCConfig.InstallServices simply tells Windsor to scan all assemblies in the /bin folder that have a name starting with WhoCanHelpMe, and find any classes in those assemblies that implement IWindsorInstaller.

### Some important notes about installers ###

When registering components with your IoC container, it's vitally important to get the lifestyle right. By default, Windsor will register everything as a Singleton; if you want anything else you have to specify it.

- WhoCanHelpMe.Presentation.Installers.ControllerInstaller - note that all controllers are registered with the PerWebRequest lifestyle. This is critically important, as the ASP.NET MVC DependencyResolver contains hook you can use to call Windsor's Release method which is used to tell Windsor that you are finished with your object. Using the PerWebRequest lifestyle ensures that you don't end up with a nasty memory leak as controller instances stack up in memory. [See here for a write up of the problem](http://mikehadlow.blogspot.co.uk/2011/02/mvc-30-idependencyresolver-interface-is.html)
- WhoCanHelpMe.Presentation.Installers.QueryInstaller - you will see that all queries (see later for more info on these) are registered with a transient lifestyle. This is because they have dependencies with non-singleton lifestyles. If you register a component with transient lifestyle, and that component is a dependency of one registered as singleton, the singleton will hold a reference to an instance of the transient service, which is probably not what was intended.
- Both the installers in WhoCanHelpMe.Presentation are examples of convention based service registration. The ControllerInstaller is searching for any classes inheriting IController, and the QueryInstaller is registering every class in a specific folder. This approach means you can set everything up at the start of the project and not have to remember to keep adding new services to your controller - it's a great way of reducing friction in your development, and I believe all of the good IoC containers have facilities in place to support this.

## Persistence and Querying ##

### Approach ###

I take the following approach when using an RDBMS:

- **Use an ORM**. They often have steep learning curves, but the best also have massive communities to help you up and tools to help you along. The alternative is writing a tonne of dull data access code, which you will almost certainly screw up at some point.
- **Use NHibernate**. For anything above "very basic", I will default to using NHibernate. Yes, the Linq provider is crap compared to Entity Framework, but the SQL it generates is light years ahead.
- **Don't abstract the ORM**. I admit being guilty of this in the past but it's a habit I have left behind me. NHibernate offers too many neat tricks you can use when querying to hide them all behind a generic interface. Future queries, eager fetching and so on can make an enormous difference to the performance of your app.
- **Keep UI queries in the UI**. Many pages in the UI will require unique combinations of bits of data from your database. It follows that they are truly UI concerns and should be treated as such. You will see all UI specific queries in the WhoCanHelpMe.Presentation.Queries namespace and by and large, each ViewModel that requires data has a corresponding Query.
- **Question the need**. Although I've left it until last, this is hugely important. Many developers will default to using an RDBMS to store their data simply because it's what we're used to. However, this is not always the best choice, especially for web apps. WCHM is a great example of an app that really should be using a document database rather than an RDBMS.

### Setup ###

NHibernate initialisation - the process of setting up the mappings between class and database table - is implemented in the infrastructure project. Those familiar with NHibernate will be aware of the approach - create your ISessionFactory at app startup (the session factory is expensive to create but thread safe, so you build one and keep it around for the duration of the app's life) and then create an ISession from the ISessionFactory when you need it - typically in a web app, this will be per web request.

In WCHM, I am using Fluent NHibernate's (FNH) AutoMapping feature. This requires the following pieces:

- AutoPersistenceModelGenerator - creates an AutoPersistenceModel (which tells FNH how to generate class mappings.) This pulls together the next three items.
- AutoMappingConfiguration - used to tell NH some basics about your model - where to find classes, how to determine what the ID field is, and so on. WCHM is mostly sticking to the FNH defaults, so there's not much code here.
- Conventions - Allow you to override specific aspects of FNH's mapping generation. We are using a single convention, which tells FNH to map all ID fields as assigned. More on this below.
- Overrides - Sometimes you need to break out of the conventions a little - in these cases, you create overrides which allow you to specify custom mapping behaviour per class.

Finally, the configuration process is wrapped up in the NHibernateInitializer class. This has the following points of interest.

- **The GetConnectionString method** - If for some reason it's not easy to normalize connection strings across development machines, this method allows you to have a connection string per developer in your web.config - the code looks for a connection string suffixed with your machine name, and if not found will fall back to a default. This is not intended to be a replacement for the standard MS web config transforms features, it's just there to make life easier for developers.
- **The configurationCallback parameter of BuildSessionFactory** - This is primarily intended to allow the integration tests to hook into the initialization process and generate a database schema directly from the model.

### Assigned or Generated IDs? ###

TBC

### Wrapping it up ###

Invocation of the NHibernate setup is done in `WhoCanHelpMe.Infrastructure.InfrastructureInstaller`. This firstly uses the `NHibernateInitializer` to build the session factory, which is added directly to the container - essentially becoming the singleton implementation of `ISessionFactory`. It then adds an ISessionFactory service to the container using Windsor's factory method facility. This allows us to specify a method that will be called when an instance of a component is required.

We do this because when an instance of ISession is required, we need to obtain it from ISessionFactory. We can't put a single instance of ISession into the container because it's not thread safe, and Windsor doesn't know how to create it directly - so we need to do it ourself. We use PerWebRequest as the lifestyle to ensure the ISession is properly disposed at the end of the web request if it's not been done before - this is established best practice for NHibernate in a web app.

## UI Optimisations ##

### CSS/JS minification and bundling ###

ASP.NET MVC 4 has a new trick up it's sleeve in this department, in the form of the `Microsoft.Web.Optimization` assembly (obtainable via NuGet) What used to be a pain is now straightforward.

It's possible to take full control over the bundling process, but WCHM doesn't need this so it's using the default bundling approach. This is specified in WhoCanHelpMe.Presentation.App_Start.BundleConfig. The references the bundles can be found in the Views/Shared/_Layout.cshtml view. The bundled stylesheet is referenced in the `<HEAD>` section (~/content/styles/css) and the bundled JS is referenced at the end of the document (again, following best practice for a speedy page load).

### jQuery via CDN ###

jQuery, Microsoft and Ajax all allow you to reference jQuery direct from their servers. This has benefits for the client - they may already have a cached version on their machine, and you - it's one less file to serve.

### Caching headers ###

TBC