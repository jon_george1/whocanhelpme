﻿namespace WhoCanHelpMe.IntegrationTests.Infrastructure.NHibernate
{
    using System;
    using System.Collections;

    public class EqualityComparers
    {
         public class DateTimeEqualityComparer : IEqualityComparer
         {
             public new bool Equals(object x, object y)
             {
                 if (x == null || y == null)
                 {
                     return false;
                 }
                 
                 if (x is DateTime && y is DateTime)
                 {
                     return ((DateTime)x).CompareTo((DateTime)y) == 1;
                 }

                 return x.Equals(y);
             }

             public int GetHashCode(object obj)
             {
                 return obj.GetHashCode();
             }
         }
    }
}