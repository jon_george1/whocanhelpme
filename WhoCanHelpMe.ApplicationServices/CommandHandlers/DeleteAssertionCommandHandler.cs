namespace WhoCanHelpMe.ApplicationServices.CommandHandlers
{
    using System;
    using System.Linq;

    using NHibernate;
    using NHibernate.Linq;

    using WhoCanHelpMe.ApplicationServices.Command;
    using WhoCanHelpMe.ApplicationServices.Contracts;
    using WhoCanHelpMe.Domain.Entities;

    public class DeleteAssertionCommandHandler : ICommandHandler<DeleteAssertionCommand>
    {
        private readonly ISession session;

        public DeleteAssertionCommandHandler(ISession session)
        {
            this.session = session;
        }

        public void Handle(DeleteAssertionCommand command)
        {
            var profile = this.session.GetProfileByUserId(command.UserId);

            var assertion = profile.Assertions.FirstOrDefault(a => a.Id == command.AssertionId);

            if (assertion == null)
            {
                throw new ArgumentException("The specified assertion does not exist.", "command");
            }

            profile.RemoveAssertion(assertion);
        }
    }
}