namespace WhoCanHelpMe.ApplicationServices.CommandHandlers
{
    using System;
    using System.Linq;

    using NHibernate;
    using NHibernate.Linq;

    using WhoCanHelpMe.Domain.Entities;

    public static class SessionExtensions
    {
        public static Profile GetProfileByUserId(this ISession session, string userId)
        {
            var profile = session.Query<Profile>().FirstOrDefault(x => x.UserId == userId);

            if (profile == null)
            {
                throw new ArgumentException("No profile exists for the specified userId", "userId");
            }
 
            return profile;
        }
    }
}