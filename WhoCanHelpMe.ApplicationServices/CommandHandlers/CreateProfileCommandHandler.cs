namespace WhoCanHelpMe.ApplicationServices.CommandHandlers
{
    using NHibernate;

    using WhoCanHelpMe.ApplicationServices.Command;
    using WhoCanHelpMe.ApplicationServices.Contracts;
    using WhoCanHelpMe.Domain.Entities;

    public class CreateProfileCommandHandler : ICommandHandler<CreateProfileCommand>
    {
        private readonly ISession session;

        public CreateProfileCommandHandler(ISession session)
        {
            this.session = session;
        }

        public void Handle(CreateProfileCommand command)
        {
            var profile = new Profile(command.ProfileId, command.UserId, command.Name);
            session.Save(profile);
        }
    }
}