namespace WhoCanHelpMe.ApplicationServices.CommandHandlers
{
    using System;
    using System.Linq;

    using NHibernate;
    using NHibernate.Linq;

    using WhoCanHelpMe.ApplicationServices.Command;
    using WhoCanHelpMe.ApplicationServices.Contracts;
    using WhoCanHelpMe.Domain.Entities;

    public class AddAssertionCommandHandler : ICommandHandler<AddAssertionCommand>
    {
        private readonly ISession session;

        public AddAssertionCommandHandler(ISession session)
        {
            this.session = session;
        }

        public void Handle(AddAssertionCommand command)
        {
            var profileQuery = this.session.Query<Profile>().Where(x => x.UserId == command.UserId).Take(1).ToFuture();
            var categoryQuery = this.session.Query<Category>().Where(x => x.Id == command.CategoryId).Take(1).ToFuture();

            var profile = profileQuery.FirstOrDefault();

            if (profile == null)
            {
                throw new ArgumentException("Profile not found", "command");
            }

            var category = categoryQuery.FirstOrDefault();
            if (category == null)
            {
                throw new ArgumentException("Category not found", "command");
            }

            profile.AddAssertion(category, command.Tag);
        }
    }
}