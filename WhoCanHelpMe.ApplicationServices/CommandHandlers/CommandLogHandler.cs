namespace WhoCanHelpMe.ApplicationServices.CommandHandlers
{
    using System.Web;

    using NHibernate;

    using WhoCanHelpMe.ApplicationServices.Contracts;
    using WhoCanHelpMe.Domain.Entities;

    public class CommandLogHandler<T> : ICommandHandler<T>
    {
        private readonly ISession session;

        public CommandLogHandler(ISession session)
        {
            this.session = session;
        }

        public void Handle(T command)
        {
            var user = HttpContext.Current == null ? string.Empty : HttpContext.Current.User.Identity.Name;

            var log = new CommandLog(user, command);

            session.Save(log);
        }
    }
}