﻿namespace WhoCanHelpMe.ApplicationServices.Contracts
{
    public interface ISecurityService
    {
        void Authenticate(string userId);

        void SignOut();
    }
}