namespace WhoCanHelpMe.ApplicationServices.Contracts
{
    public interface ICommandHandler<T>
    {
        void Handle(T command);
    }
}