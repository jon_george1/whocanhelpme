namespace WhoCanHelpMe.ApplicationServices.Contracts
{
    public interface IBus
    {
        void Send<T>(T command);
    }
}