namespace WhoCanHelpMe.ApplicationServices
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;

    using WhoCanHelpMe.ApplicationServices.CommandHandlers;

    public class ApplicationServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes.FromThisAssembly()
                       .InSameNamespaceAs<CreateProfileCommandHandler>()
                       .WithService
                       .FirstInterface()
                       .LifestyleTransient());
        }
    }
}