namespace WhoCanHelpMe.ApplicationServices.Command
{
    using System;

    public class DeleteAssertionCommand
    {
        public string UserId { get; set; }

        public Guid AssertionId { get; set; }
    }
}