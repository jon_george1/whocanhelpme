namespace WhoCanHelpMe.ApplicationServices.Command
{
    using System;

    public class AddAssertionCommand
    {
        public string UserId { get; set; }

        public Guid CategoryId { get; set; }

        public string Tag { get; set; }
    }
}