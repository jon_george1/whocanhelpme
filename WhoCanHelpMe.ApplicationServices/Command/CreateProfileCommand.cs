namespace WhoCanHelpMe.ApplicationServices.Command
{
    using System;

    public class CreateProfileCommand
    {
        public CreateProfileCommand()
        {
            this.ProfileId = Guid.NewGuid();
        }

        public Guid ProfileId { get; set; }

        public string Name { get; set; }

        public string UserId { get; set; }
    }
}