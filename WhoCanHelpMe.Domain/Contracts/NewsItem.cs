﻿namespace WhoCanHelpMe.Domain.Contracts
{
    using System;
    using System.Diagnostics;

    [DebuggerDisplay("{Headline}")]
    public class NewsItem
    {
        public virtual string Author { get; set; }

        public virtual string AuthorPhotoUrl { get; set; }

        public virtual string AuthorUrl { get; set; }

        public virtual string Headline { get; set; }

        public string Id { get; set; }

        public virtual DateTime PublishedTime { get; set; }

        public virtual string Url { get; set; }

        public string AuthorId { get; set; }
    }
}