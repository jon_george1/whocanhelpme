namespace WhoCanHelpMe.Domain.Contracts
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface INewsService
    {
        Task<IEnumerable<NewsItem>> GetProjectBuzzAsync();

        Task<IEnumerable<NewsItem>> GetDevelopmentTeamBuzzAsync();
    }
}