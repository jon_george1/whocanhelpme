namespace WhoCanHelpMe.Domain.Entities
{
    using System;

    using Newtonsoft.Json;

    public class CommandLog
    {
        public CommandLog(string userName, object command)
        {
            this.UserName = userName;

            this.ProcessedOn = DateTime.UtcNow;

            this.CommandName = command.GetType().Name;
            this.Data = JsonConvert.SerializeObject(command, Formatting.Indented);
        }

        protected CommandLog()
        {
        }

        public virtual Guid Id { get; protected set; }

        public virtual string UserName { get; protected set; }

        public virtual string Data { get; protected set; }

        public virtual string CommandName { get; protected set; }

        public virtual DateTime ProcessedOn { get; protected set; }
    }
}