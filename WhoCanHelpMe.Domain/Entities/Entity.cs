﻿namespace WhoCanHelpMe.Domain.Entities
{
    using System;

    public abstract class Entity
    {
        protected Entity(Guid id)
        {
            this.Id = id;
        }

        public virtual Guid Id { get; protected set; }

        public virtual int Version { get; protected set; }
    }
}