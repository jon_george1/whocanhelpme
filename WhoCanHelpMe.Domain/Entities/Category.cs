namespace WhoCanHelpMe.Domain.Entities
{
    using System;
    using System.Diagnostics;

    [DebuggerDisplay("{Name}")]
    public class Category : Entity
    {
        public Category(Guid id, string name, string description) : base(id)
        {
            this.Name = name;
            this.Description = description;
        }

        protected Category() : base(Guid.NewGuid())
        {
        }

        public virtual string Description { get; protected set; }

        public virtual string Name { get; protected set; }
    }
}