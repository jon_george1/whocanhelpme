﻿namespace WhoCanHelpMe.Domain.Entities
{
    using System;
    using System.Diagnostics;

    [DebuggerDisplay("{Profile} {Category} {Tag}")]
    public class Assertion : Entity
    {
        public Assertion(Guid id, Category category, Profile profile, string tag) : base(id)
        {
            this.Category = category;
            this.Profile = profile;
            this.Tag = tag;
        }

        protected Assertion() : base(Guid.NewGuid())
        {
        }

        public virtual Category Category { get; protected set; }

        public virtual Profile Profile { get; protected set; }

        public virtual string Tag { get; protected set; }
    }
}