﻿namespace WhoCanHelpMe.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    [DebuggerDisplay("{Name} - {UserName}")]
    public class Profile : Entity
    {
        public Profile(Guid id, string userId, string name) : base(id)
        {
            this.UserId = userId;
            this.Name = name;
            this.Assertions = new List<Assertion>();
            this.CreatedOn = DateTime.UtcNow;
        }

        protected Profile()
            : base(Guid.NewGuid())
        {
        }

        public virtual IList<Assertion> Assertions { get; protected set; }

        public virtual DateTime CreatedOn { get; protected set; }

        public virtual string Name { get; protected set; }

        public virtual string UserId { get; protected set; }

        public virtual void AddAssertion(Category category, string tag)
        {
            this.Assertions.Add(new Assertion(Guid.NewGuid(), category, this, tag));
        }

        public virtual void RemoveAssertion(Assertion assertion)
        {
            this.Assertions.Remove(assertion);
        }
    }
}