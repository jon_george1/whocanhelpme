namespace WhoCanHelpMe.Infrastructure.NHibernate
{
    using FluentNHibernate.Automapping;

    using WhoCanHelpMe.Domain.Entities;

    public class AutomappingConfiguration : DefaultAutomappingConfiguration
    {
        private readonly string entityNamespace;

        public AutomappingConfiguration()
        {
            this.entityNamespace = typeof(Profile).Namespace;
        }

        public override bool ShouldMap(System.Type type)
        {
            return type.Namespace == this.entityNamespace;
        }
    }
}