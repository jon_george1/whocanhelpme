namespace WhoCanHelpMe.Infrastructure.NHibernate
{
    using FluentNHibernate.Automapping;

    using WhoCanHelpMe.Domain.Entities;

    public class AutoPersistenceModelGenerator
    {
        public static AutoPersistenceModel Create()
        {
            var model = AutoMap.AssemblyOf<Profile>(new AutomappingConfiguration());
            model.UseOverridesFromAssemblyOf<AutoPersistenceModelGenerator>();
            model.Conventions.AddFromAssemblyOf<AutoPersistenceModelGenerator>();
            return model;
        }
    }
}