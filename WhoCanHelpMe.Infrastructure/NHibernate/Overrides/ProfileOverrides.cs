﻿namespace WhoCanHelpMe.Infrastructure.NHibernate.Overrides
{
    using FluentNHibernate.Automapping;
    using FluentNHibernate.Automapping.Alterations;

    using WhoCanHelpMe.Domain.Entities;

    public class ProfileOverrides : IAutoMappingOverride<Profile>
    {
        public void Override(AutoMapping<Profile> mapping)
        {
            mapping.HasMany(m => m.Assertions).Cascade.AllDeleteOrphan().Inverse().Not.LazyLoad();
        }
    }
}