namespace WhoCanHelpMe.Infrastructure.NHibernate.Overrides
{
    using FluentNHibernate.Automapping;
    using FluentNHibernate.Automapping.Alterations;

    using WhoCanHelpMe.Domain.Entities;

    public class CommandLogOverrides : IAutoMappingOverride<CommandLog>
    {
        public void Override(AutoMapping<CommandLog> mapping)
        {
            mapping.Id(x => x.Id).GeneratedBy.GuidComb();
        }
    }
}