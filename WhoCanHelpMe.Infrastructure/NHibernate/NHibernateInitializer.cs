﻿namespace WhoCanHelpMe.Infrastructure.NHibernate
{
    using System;
    using System.Configuration;

    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;

    using global::NHibernate;

    public static class NHibernateInitializer
    {
        public static ISessionFactory BuildSessionFactory()
        {
            return BuildSessionFactory(x => { });
        }

        public static ISessionFactory BuildSessionFactory(Action<FluentConfiguration> configurationCallback)
        {
            HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();

            var fluentConfiguration =
                Fluently.Configure().Database(
                    MsSqlConfiguration.MsSql2008.ConnectionString(c => c.Is(GetConnectionString()))).Mappings(
                        c => c.AutoMappings.Add(AutoPersistenceModelGenerator.Create()));

            configurationCallback(fluentConfiguration);

            var configuration = fluentConfiguration.BuildConfiguration();

            return configuration.BuildSessionFactory();
        }

        private static string GetConnectionString()
        {
            const string BaseKey = "wchm";
            var localKey = string.Format("{0}-{1}", BaseKey, Environment.MachineName);

            var connectionString = ConfigurationManager.ConnectionStrings[localKey];

            if (connectionString == null)
            {
                connectionString = ConfigurationManager.ConnectionStrings[BaseKey];
            }

            return connectionString.ConnectionString;
        }
    }
}