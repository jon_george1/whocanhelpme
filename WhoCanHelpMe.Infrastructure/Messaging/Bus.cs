namespace WhoCanHelpMe.Infrastructure.Messaging
{
    using System;

    using Castle.Windsor;

    using WhoCanHelpMe.ApplicationServices.Contracts;

    public class Bus : IBus
    {
        private readonly IWindsorContainer container;

        public Bus(IWindsorContainer container)
        {
            this.container = container;
        }

        public void Send<T>(T args)
        {
            var handlers = this.container.ResolveAll<ICommandHandler<T>>();

            if (handlers.Length == 0)
            {
                throw new ArgumentException("No handlers were found for command type " + typeof(T).FullName, "args");
            }

            foreach (var handler in handlers)
            {
                handler.Handle(args);
            }
        }
    }
}