namespace WhoCanHelpMe.Infrastructure.Caching
{
    using System;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Caching;

    public static class CacheHelper
    {
        public static T GetResultWithCache<T>(string key, int cacheDuractionSeconds, Func<T> targetFunction) where T : class
        {
            var value = HttpRuntime.Cache[key] as T;

            if (value == null)
            {
                value = targetFunction();

                HttpRuntime.Cache.Add(
                    key,
                    value,
                    null,
                    DateTime.Now.AddSeconds(cacheDuractionSeconds),
                    Cache.NoSlidingExpiration,
                    CacheItemPriority.Normal,
                    null);
            }

            return HttpRuntime.Cache[key] as T;
        }

        public static Task<T> GetAsyncResultWithCache<T>(
            string key, int cacheDuractionSeconds, Func<Task<T>> targetFunction) where T : class
        {
            var value = HttpRuntime.Cache[key] as T;

            if (value != null)
            {
                var cacheTask = new Task<T>(() => value);
                cacheTask.Start();
                return cacheTask;
            }

            var task = targetFunction();
            return task.ContinueWith(
                x =>
                {
                    HttpRuntime.Cache.Add(
                        key,
                        x.Result,
                        null,
                        DateTime.Now.AddSeconds(cacheDuractionSeconds),
                        Cache.NoSlidingExpiration,
                        CacheItemPriority.Normal,
                        null);

                    return x.Result;
                });
        }
    }
}