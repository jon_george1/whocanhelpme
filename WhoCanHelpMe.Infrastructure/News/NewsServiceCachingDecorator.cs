namespace WhoCanHelpMe.Infrastructure.News
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Caching;

    using WhoCanHelpMe.Domain.Contracts;
    using WhoCanHelpMe.Infrastructure.Caching;

    public class NewsServiceCachingDecorator : INewsService
    {
        private readonly INewsService decorated;

        public NewsServiceCachingDecorator(INewsService decorated)
        {
            this.decorated = decorated;
        }

        public Task<IEnumerable<NewsItem>> GetDevelopmentTeamBuzzAsync()
        {
            return CacheHelper.GetResultWithCache("GetDevelopmentTeamBuzz", 300, this.decorated.GetDevelopmentTeamBuzzAsync);
        }

        public Task<IEnumerable<NewsItem>> GetProjectBuzzAsync()
        {
            return CacheHelper.GetResultWithCache("GetProjectBuzzAsync", 300, this.decorated.GetProjectBuzzAsync);
        }
    }
}