namespace WhoCanHelpMe.Infrastructure.News
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using LinqToTwitter;

    using WhoCanHelpMe.Domain.Contracts;
    using WhoCanHelpMe.Infrastructure.News.Configuration;

    public class TwitterNewsService : INewsService
    {
        private readonly NewsConfigurationSection newsConfiguration;

        public TwitterNewsService(NewsConfigurationSection newsConfiguration)
        {
            this.newsConfiguration = newsConfiguration;
        }

        public Task<IEnumerable<NewsItem>> GetDevelopmentTeamBuzzAsync()
        {
            var task = new Task<IEnumerable<NewsItem>>(() =>
                {
                    var tasks = this.newsConfiguration.DevTeamHeadlineTags.Cast<SearchTag>().Select(x => this.ByScreenName(x.Name));

                    Task.WaitAll(tasks.ToArray());

                    var orderedTweets = tasks.SelectMany(x => x.Result)
                        .OrderByDescending(x => x.CreatedAt)
                        .ToList();

                    var results = orderedTweets
                                          .Take(this.newsConfiguration.NoOfDevTeamHeadlines)
                                          .Select(MapToNewsItem);
                    return results;
                });

            task.Start();
            return task;
        }

        public Task<List<Status>> ByScreenName(string screenName)
        {
            var task = new Task<List<Status>>(() =>
                {
                    try
                    {
                        using (var context = new TwitterContext())
                        {
                            var results = from search in context.Status
                                          where search.Type == StatusType.User &&
                                                search.ScreenName == screenName &&
                                                search.Count == this.newsConfiguration.NoOfDevTeamHeadlines
                                          select search;

                            return results.ToList();
                        }
                    }
                    catch (Exception)
                    {
                        // TODO: Log this
                    }

                    return new List<Status>();
                });

            task.Start();
            return task;
        }

        public Task<List<SearchEntry>> ByHashTag(string hashTag)
        {
            var task = new Task<List<SearchEntry>>(() =>
                {
                    try
                    {
                        using (var context = new TwitterContext())
                        {
                            var results = from search in context.Search
                                          where search.Type == SearchType.Search &&
                                                search.Hashtag == hashTag &&
                                                search.PageSize == this.newsConfiguration.NoOfBuzzHeadlines
                                          select search;

                            return results.Single().Results.ToList();
                        }
                    }
                    catch (Exception)
                    {
                        // TODO: Log this
                    }

                    return new List<SearchEntry>();
                });

            task.Start();
            return task;
        }

        public static NewsItem MapToNewsItem(Status entry)
        {
            return new NewsItem
                {
                    Author = entry.User.Name,
                    AuthorPhotoUrl = entry.User.ProfileImageUrl,
                    AuthorId = entry.User.Identifier.ScreenName,
                    AuthorUrl = "https://twitter.com/" + entry.User.Identifier.ScreenName,
                    Headline = entry.Text,
                    Id = entry.ID,
                    PublishedTime = entry.CreatedAt,
                    Url = string.Format("https://twitter.com/{0}/status/{1}", entry.User.Identifier.ScreenName, entry.ID)
                };
        }

        public static NewsItem MapToNewsItem(SearchEntry entry)
        {
            return new NewsItem
                {
                    Author = entry.FromUserName,
                    AuthorPhotoUrl = entry.ProfileImageUrl,
                    AuthorId = entry.FromUser,
                    AuthorUrl = "https://twitter.com/" + entry.FromUser,
                    Headline = entry.Text,
                    Id = entry.ID.ToString(),
                    PublishedTime = entry.CreatedAt.DateTime,
                    Url = string.Format("https://twitter.com/{0}/status/{1}", entry.FromUser, entry.ID)
                };
        }

        public Task<IEnumerable<NewsItem>> GetProjectBuzzAsync()
        {
            var task = new Task<IEnumerable<NewsItem>>(() =>
            {
                var tasks = this.newsConfiguration.BuzzHeadlineTags.Cast<SearchTag>().Select(x => this.ByHashTag(x.Name));

                Task.WaitAll(tasks.ToArray());

                var orderedTweets = tasks.SelectMany(x => x.Result)
                    .OrderByDescending(x => x.CreatedAt)
                    .ToList();

                var results = orderedTweets
                                      .Take(this.newsConfiguration.NoOfDevTeamHeadlines)
                                      .Select(MapToNewsItem);
                return results;
            });

            task.Start();
            return task;
        }
    }
}