﻿namespace WhoCanHelpMe.Infrastructure.News.Configuration
{
    using System;
    using System.Configuration;

    public class SearchTag : ConfigurationElement
    {
        internal const String NamePropertyName = "name";

        [ConfigurationProperty(NamePropertyName, IsRequired = true, IsKey = true, IsDefaultCollection = false)]
        public String Name
        {
            get
            {
                return (String)base[NamePropertyName];
            }
            set
            {
                base[NamePropertyName] = value;
            }
        }
    }
}