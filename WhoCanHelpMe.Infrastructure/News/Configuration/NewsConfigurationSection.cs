﻿namespace WhoCanHelpMe.Infrastructure.News.Configuration
{
    using System;
    using System.Configuration;

    public class NewsConfigurationSection : ConfigurationSection
    {
        internal const String BuzzHeadlineTagsPropertyName = "buzzHeadlineTags";

        internal const String DevTeamHeadlineTagsPropertyName = "devTeamHeadlineTags";

        internal const String NewsConfigurationSectionSectionName = "newsConfigurationSection";

        internal const String NoOfBuzzHeadlinesPropertyName = "noOfBuzzHeadlines";

        internal const String NoOfDevTeamHeadlinesPropertyName = "noOfDevTeamHeadlines";

        internal const String SearchTimeoutSecondsPropertyName = "searchTimeoutSeconds";

        internal const String XmlnsPropertyName = "xmlns";

        public static NewsConfigurationSection Instance
        {
            get
            {
                return ConfigurationManager.GetSection(NewsConfigurationSectionSectionName) as NewsConfigurationSection;
            }
        }

        [ConfigurationProperty(BuzzHeadlineTagsPropertyName, IsRequired = false, IsKey = false,
            IsDefaultCollection = false)]
        public SearchTags BuzzHeadlineTags
        {
            get
            {
                return (SearchTags)base[BuzzHeadlineTagsPropertyName];
            }
            set
            {
                base[BuzzHeadlineTagsPropertyName] = value;
            }
        }

        [ConfigurationProperty(DevTeamHeadlineTagsPropertyName, IsRequired = false, IsKey = false,
            IsDefaultCollection = false)]
        public SearchTags DevTeamHeadlineTags
        {
            get
            {
                return (SearchTags)base[DevTeamHeadlineTagsPropertyName];
            }
            set
            {
                base[DevTeamHeadlineTagsPropertyName] = value;
            }
        }

        [ConfigurationProperty(NoOfBuzzHeadlinesPropertyName, IsRequired = false, IsKey = false,
            IsDefaultCollection = false, DefaultValue = 7)]
        public Int32 NoOfBuzzHeadlines
        {
            get
            {
                return (Int32)base[NoOfBuzzHeadlinesPropertyName];
            }
            set
            {
                base[NoOfBuzzHeadlinesPropertyName] = value;
            }
        }

        [ConfigurationProperty(NoOfDevTeamHeadlinesPropertyName, IsRequired = false, IsKey = false,
            IsDefaultCollection = false, DefaultValue = 7)]
        public Int32 NoOfDevTeamHeadlines
        {
            get
            {
                return (Int32)base[NoOfDevTeamHeadlinesPropertyName];
            }
            set
            {
                base[NoOfDevTeamHeadlinesPropertyName] = value;
            }
        }

        [ConfigurationProperty(SearchTimeoutSecondsPropertyName, IsRequired = false, IsKey = false,
            IsDefaultCollection = false, DefaultValue = 10)]
        public Int32 SearchTimeoutSeconds
        {
            get
            {
                return (Int32)base[SearchTimeoutSecondsPropertyName];
            }
            set
            {
                base[SearchTimeoutSecondsPropertyName] = value;
            }
        }

        [ConfigurationProperty(XmlnsPropertyName, IsRequired = false, IsKey = false, IsDefaultCollection = false)]
        public String Xmlns
        {
            get
            {
                return (String)base[XmlnsPropertyName];
            }
        }
    }
}