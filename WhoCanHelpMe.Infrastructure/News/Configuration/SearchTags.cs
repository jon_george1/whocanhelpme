﻿namespace WhoCanHelpMe.Infrastructure.News.Configuration
{
    using System;
    using System.Configuration;

    /// <summary>
    ///   A collection of SearchTag instances.
    /// </summary>
    [ConfigurationCollection(typeof(SearchTag), CollectionType = ConfigurationElementCollectionType.BasicMapAlternate, AddItemName = SearchTagPropertyName)]
    public class SearchTags : ConfigurationElementCollection
    {
        internal const String SearchTagPropertyName = "searchTag";

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMapAlternate;
            }
        }

        public SearchTag this[int index]
        {
            get
            {
                return (SearchTag)this.BaseGet(index);
            }
        }

        public void Add(SearchTag searchTag)
        {
            base.BaseAdd(searchTag);
        }

        public void Remove(SearchTag searchTag)
        {
            base.BaseRemove(searchTag);
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new SearchTag();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((SearchTag)element).Name;
        }

        protected override Boolean IsElementName(String elementName)
        {
            return (elementName == SearchTagPropertyName);
        }
    }
}