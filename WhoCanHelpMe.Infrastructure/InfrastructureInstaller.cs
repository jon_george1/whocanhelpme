﻿namespace WhoCanHelpMe.Infrastructure
{
    using Castle.Facilities.FactorySupport;
    using Castle.MicroKernel;
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;

    using WhoCanHelpMe.Domain.Contracts;
    using WhoCanHelpMe.Infrastructure.Messaging;
    using WhoCanHelpMe.Infrastructure.News;
    using WhoCanHelpMe.Infrastructure.News.Configuration;
    using WhoCanHelpMe.Infrastructure.NHibernate;

    using global::NHibernate;

    using WhoCanHelpMe.ApplicationServices.Contracts;
    using WhoCanHelpMe.Infrastructure.Security;

    public class InfrastructureInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            RegisterSecurityServices(container);

            RegisterNHibernateSessionFactory(container);
            RegisterNHibernateSession(container);

            RegisterMessageBus(container);

            RegisterNewsServices(container);
        }

        private static void RegisterMessageBus(IWindsorContainer container)
        {
            container.Register(Component.For<IBus>().ImplementedBy<Bus>());
        }

        private static void RegisterNHibernateSessionFactory(IWindsorContainer container)
        {
            var sessionFactory = NHibernateInitializer.BuildSessionFactory();
            container.Register(Component.For<ISessionFactory>().Instance(sessionFactory));
        }

        private static void RegisterNHibernateSession(IWindsorContainer container)
        {
            container.AddFacility<FactorySupportFacility>();

            container.Register(Component.For<ISession>().LifeStyle.PerWebRequest.UsingFactoryMethod(kernel => kernel.Resolve<ISessionFactory>().OpenSession()));
        }

        private static void RegisterSecurityServices(IWindsorContainer container)
        {
            container.Register(Component.For<ISecurityService>().ImplementedBy<SecurityService>());
        }

        private static void RegisterNewsServices(IWindsorContainer container)
        {
            container.Register(Component.For<NewsConfigurationSection>().Instance(NewsConfigurationSection.Instance));
            container.Register(Component.For<INewsService>().ImplementedBy<NewsServiceCachingDecorator>());
            container.Register(Component.For<INewsService>().ImplementedBy<TwitterNewsService>());
        }
    }
}