﻿namespace WhoCanHelpMe.Presentation.Installers
{
    using System.Linq;
    using System.Reflection;
    using System.Web.Mvc;

    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;

    public class ControllerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var controllers = Assembly.GetExecutingAssembly().GetExportedTypes().Where(x => typeof(IController).IsAssignableFrom(x));

            foreach (var current in controllers)
            {
                // Register controllers with per web request lifestyle to avoid issue with MVC dependency resolver not releasing components.
                container.Register(Component.For(current).Named(current.FullName.ToLowerInvariant()).LifestylePerWebRequest());
            }
        }
    }
}