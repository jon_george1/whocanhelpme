namespace WhoCanHelpMe.Presentation.Installers
{
    using System;

    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;

    using WhoCanHelpMe.Presentation.Queries;

    public class QueryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly().InSameNamespaceAs<UpdateProfileViewModelQuery>().WithServiceFirstInterface().LifestyleTransient());
        }
    }
}