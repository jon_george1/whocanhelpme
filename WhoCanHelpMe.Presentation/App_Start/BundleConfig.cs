[assembly: WebActivator.PostApplicationStartMethod(typeof(WhoCanHelpMe.Presentation.App_Start.BundleConfig), "RegisterBundles")]

namespace WhoCanHelpMe.Presentation.App_Start
{
    using Microsoft.Web.Optimization;

    public static class BundleConfig
    {
        public static void RegisterBundles()
        {
            BundleTable.Bundles.EnableDefaultBundles();
        }
    }
}