﻿[assembly: WebActivator.PostApplicationStartMethod(typeof(WhoCanHelpMe.Presentation.App_Start.FilterConfig), "RegisterGlobalFilters")]

namespace WhoCanHelpMe.Presentation.App_Start
{
    using System.Web.Mvc;

    public static class FilterConfig
    {
        public static void RegisterGlobalFilters()
        {
            var filters = GlobalFilters.Filters;
            filters.Add(new HandleErrorAttribute());
        }
    }
}