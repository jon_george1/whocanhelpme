﻿[assembly: WebActivator.PreApplicationStartMethod(typeof(WhoCanHelpMe.Presentation.App_Start.IoCConfig), "InitialiseIocContainer")]

namespace WhoCanHelpMe.Presentation.App_Start
{
    using System.Linq;
    using System.Web.Http;
    using System.Web.Mvc;

    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using Castle.Windsor.Installer;

    public class IoCConfig
    {
        public static IWindsorContainer InitialiseIocContainer()
        {
            var container = new WindsorContainer();
            container.Register(Component.For<IWindsorContainer>().Instance(container));

            SetDependencyResolver(container);

            InstallServices(container);

            return container;
        }

        private static void InstallServices(IWindsorContainer container)
        {
            var installers = FromAssembly.InDirectory(new AssemblyFilter("bin", "WhoCanHelpMe*.dll"));
            container.Install(installers);
        }

        private static void SetDependencyResolver(IWindsorContainer container)
        {
            DependencyResolver.SetResolver(
                x => container.Kernel.HasComponent(x) ? container.Resolve(x) : null, 
                x => container.Kernel.HasComponent(x) ? container.ResolveAll(x).Cast<object>() : new object[0]);
        }
    }
}