namespace WhoCanHelpMe.Presentation.Queries
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    using NHibernate;
    using NHibernate.Linq;

    using WhoCanHelpMe.Domain.Entities;
    using WhoCanHelpMe.Presentation.Models;

    public class UpdateProfileViewModelQuery : IViewModelQuery<UpdateProfileViewModel>
    {
        private readonly ISession session;

        public UpdateProfileViewModelQuery(ISession session)
        {
            this.session = session;
        }

        public UpdateProfileViewModel Get(dynamic parameters)
        {
            if (string.IsNullOrEmpty(parameters.UserId))
            {
                throw new ArgumentException("No user id was specified", "parameters");
            }
            var userId = (string)parameters.UserId;

            var profiles =
                this.session.Query<Profile>().FetchMany(x => x.Assertions).ThenFetch(a => a.Category).Where(
                    p => p.UserId == userId).ToFuture();

            var categories = this.session.Query<Category>().ToFuture();

            var profile = profiles.FirstOrDefault();

            if (profile == null)
            {
                return null;
            }

            var viewModel = new UpdateProfileViewModel
                {
                    Categories = categories.Select(MapCategory),
                    Name = profile.Name,
                    Assertions = profile.Assertions.Select(MapAssertion).ToList()

                };

            return viewModel;
        }

        private static AssertionViewModel MapAssertion(Assertion input)
        {
            return new AssertionViewModel
                {
                    Id = input.Id,
                    CategoryName = input.Category.Name,
                    Tag = input.Tag
                };
        }

        private static SelectListItem MapCategory(Category input)
        {
            return new SelectListItem { Text = input.Name, Value = input.Id.ToString() };
        }
    }
}