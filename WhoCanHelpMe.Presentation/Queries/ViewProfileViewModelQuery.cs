namespace WhoCanHelpMe.Presentation.Queries
{
    using System;
    using System.Linq;

    using NHibernate;

    using WhoCanHelpMe.Domain.Entities;
    using WhoCanHelpMe.Presentation.Models;

    public class ViewProfileViewModelQuery : IViewModelQuery<ViewProfileViewModel>
    {
        private readonly ISession session;

        public ViewProfileViewModelQuery(ISession session)
        {
            this.session = session;
        }

        public ViewProfileViewModel Get(dynamic parameters)
        {
            if (parameters.ProfileId == null)
            {
                throw new ArgumentException("No profile id was specified", "parameters");
            }

            var profileId = (Guid)parameters.ProfileId;

            var profile = this.session.Get<Profile>(profileId);
                
            if (profile == null)
            {
                return null;
            }

            var viewModel = new ViewProfileViewModel
                {
                    Name = profile.Name,
                    Assertions = profile.Assertions.Select(MapAssertion).ToList()

                };

            return viewModel;
        }

        private static AssertionViewModel MapAssertion(Assertion input)
        {
            return new AssertionViewModel
                {
                    Id = input.Id,
                    CategoryName = input.Category.Name,
                    Tag = input.Tag
                };
        }
    }
}