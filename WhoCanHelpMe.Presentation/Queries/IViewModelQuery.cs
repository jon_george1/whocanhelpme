﻿namespace WhoCanHelpMe.Presentation.Queries
{
    public interface IViewModelQuery<T>
    {
        T Get(dynamic parameters);
    }
}