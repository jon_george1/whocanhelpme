namespace WhoCanHelpMe.Presentation.Queries
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;

    using NHibernate;
    using NHibernate.Linq;

    using WhoCanHelpMe.Domain.Entities;
    using WhoCanHelpMe.Presentation.Models;

    public class SearchByTagResultsViewModelQuery : IViewModelQuery<SearchByTagResultsViewModel>
    {
        private readonly ISession session;

        public SearchByTagResultsViewModelQuery(ISession session)
        {
            this.session = session;
        }

        public SearchByTagResultsViewModel Get(dynamic parameters)
        {
            var tagName = ((string)parameters.tagName).ToLowerInvariant();

            if (string.IsNullOrEmpty(tagName))
            {
                throw new ArgumentException("No tag name was specified");
            }

            var matchingAssertions = this.session.Query<Assertion>().Fetch(x => x.Profile).Fetch(x => x.Category).Where(a => a.Tag.ToLowerInvariant() == tagName);

            var data = matchingAssertions.Select(MapAssertionToSearchByTagResultViewModel).ToList();

            return new SearchByTagResultsViewModel
                {
                    SearchTerm = parameters.tagName,
                    Results = new Collection<SearchByTagResultViewModel>(data)
                };
        }

        private static SearchByTagResultViewModel MapAssertionToSearchByTagResultViewModel(Assertion assertion)
        {
            return new SearchByTagResultViewModel
                {
                    CategoryName = assertion.Category.Name,
                    ProfileId = assertion.Profile.Id,
                    ProfileName = assertion.Profile.Name,
                    Tag = assertion.Tag
                };
        }
    }
}