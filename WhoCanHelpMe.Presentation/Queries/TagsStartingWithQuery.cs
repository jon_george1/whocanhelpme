namespace WhoCanHelpMe.Presentation.Queries
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using NHibernate;
    using NHibernate.Linq;

    using WhoCanHelpMe.Domain.Entities;

    public class TagsStartingWithQuery : IViewModelQuery<IEnumerable<string>>
    {
        private readonly ISession session;

        public TagsStartingWithQuery(ISession session)
        {
            this.session = session;
        }

        public IEnumerable<string> Get(dynamic parameters)
        {
            var query = (string)parameters.query;

            if (string.IsNullOrEmpty(query))
            {
                throw new ArgumentException("The query is not specified", "parameters");
            }

            var tags = this.session.Query<Assertion>()
                                   .Where(a => a.Tag.StartsWith(query))
                                   .Select(a => a.Tag)
                                   .Distinct()
                                   .ToList();

            tags.Sort();

            return tags;
        }
    }
}