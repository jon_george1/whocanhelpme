﻿namespace WhoCanHelpMe.Presentation.ActionFilters
{
    using System.Web.Mvc;

    using NHibernate;

    public class TransactionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var session = DependencyResolver.Current.GetService<ISession>();
            session.BeginTransaction();
        }


        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var session = DependencyResolver.Current.GetService<ISession>();
            
            if (filterContext.Exception == null)
            {
                session.Transaction.Commit();
            }
            else
            {
                session.Transaction.Rollback();
            }

            session.Close();
            session.Dispose();
        }
    }
}