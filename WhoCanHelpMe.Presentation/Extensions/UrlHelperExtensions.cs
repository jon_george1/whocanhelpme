﻿namespace WhoCanHelpMe.Presentation.Extensions
{
    using System.Web.Mvc;

    public static class UrlHelperExtensions
    {
        public static string Image(this UrlHelper helper, string imageName)
        {
            return helper.Content("~/content/images/" + imageName.ToLowerInvariant());
        }

        public static string Script(this UrlHelper helper, string scriptName)
        {
            return helper.Content("~/scripts/" + scriptName.ToLowerInvariant());
        }

        public static string Stylesheet(this UrlHelper helper, string stylesheetName)
        {
            return helper.Content("~/content/styles/" + stylesheetName.ToLowerInvariant());
        }
    }
}