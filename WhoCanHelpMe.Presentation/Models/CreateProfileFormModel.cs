namespace WhoCanHelpMe.Presentation.Models
{
    using System.ComponentModel.DataAnnotations;

    public class CreateProfileFormModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
}