namespace WhoCanHelpMe.Presentation.Models
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class UpdateProfileViewModel
    {
        public UpdateProfileViewModel()
        {
            this.FormModel = new AddAssertionFormModel();
        }

        public string Name { get; set; }

        public List<AssertionViewModel> Assertions { get; set; }

        public IEnumerable<SelectListItem> Categories { get; set; }

        public AddAssertionFormModel FormModel { get; set; }
    }
}