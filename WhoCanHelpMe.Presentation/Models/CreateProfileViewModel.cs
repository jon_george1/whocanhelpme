﻿namespace WhoCanHelpMe.Presentation.Models
{
    public class CreateProfileViewModel
    {
        public CreateProfileViewModel()
        {
            this.FormModel = new CreateProfileFormModel();
        }

        public CreateProfileFormModel FormModel { get; set; }
    }
}