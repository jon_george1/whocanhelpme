namespace WhoCanHelpMe.Presentation.Models
{
    using System.Collections.ObjectModel;

    public class SearchByTagResultsViewModel
    {
        public SearchByTagResultsViewModel()
        {
            this.Results = new Collection<SearchByTagResultViewModel>();
        }

        public string SearchTerm { get; set; }

        public Collection<SearchByTagResultViewModel> Results { get; set; }
    }
}