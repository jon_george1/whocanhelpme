﻿namespace WhoCanHelpMe.Presentation.Models
{
    using System;

    public class AssertionViewModel
    {
        public Guid Id { get; set; }

        public string CategoryName { get; set; }

        public string Tag { get; set; }
    }
}