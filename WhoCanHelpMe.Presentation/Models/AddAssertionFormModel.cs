namespace WhoCanHelpMe.Presentation.Models
{
    using System;

    public class AddAssertionFormModel
    {
        public Guid CategoryId { get; set; }

        public string Tag { get; set; }
    }
}