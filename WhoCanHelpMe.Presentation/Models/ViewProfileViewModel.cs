﻿namespace WhoCanHelpMe.Presentation.Models
{
    using System.Collections.Generic;

    public class ViewProfileViewModel
    {
        public string Name { get; set; }

        public List<AssertionViewModel> Assertions { get; set; }
    }
}