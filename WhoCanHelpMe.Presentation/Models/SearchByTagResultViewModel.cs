﻿namespace WhoCanHelpMe.Presentation.Models
{
    using System;

    public class SearchByTagResultViewModel
    {
        public Guid ProfileId { get; set; }

        public string ProfileName { get; set; }

        public string CategoryName { get; set; }

        public string Tag { get; set; }
    }
}