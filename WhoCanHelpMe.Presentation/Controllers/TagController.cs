namespace WhoCanHelpMe.Presentation.Controllers
{
    using System.Collections.Generic;
    using System.Text;
    using System.Web.Mvc;

    using WhoCanHelpMe.Presentation.ActionFilters;
    using WhoCanHelpMe.Presentation.Queries;

    public class TagController : Controller
    {
        private readonly IViewModelQuery<IEnumerable<string>> tagsStartingWithQuery;

        public TagController(IViewModelQuery<IEnumerable<string>> tagsStartingWithQuery)
        {
            this.tagsStartingWithQuery = tagsStartingWithQuery;
        }

        [HttpGet]
        [Transaction]
        public ActionResult StartingWith(string q)
        {
            var data = this.tagsStartingWithQuery.Get(new { query = q});
            return new ContentResult
                {
                    Content = string.Join("\n", data),
                    ContentType = "text/plain",
                    ContentEncoding = Encoding.Default
                };
        }
    }
}