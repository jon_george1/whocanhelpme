namespace WhoCanHelpMe.Presentation.Controllers
{
    using System.Web.Mvc;

    using WhoCanHelpMe.Presentation.Models;
    using WhoCanHelpMe.Presentation.Queries;

    public class SearchController : Controller
    {
        private readonly IViewModelQuery<SearchByTagResultsViewModel> searchByTagQuery;

        public SearchController(IViewModelQuery<SearchByTagResultsViewModel> searchByTagQuery)
        {
            this.searchByTagQuery = searchByTagQuery;
        }

        public ActionResult ByTag(string tagName)
        {
            var viewModel = this.searchByTagQuery.Get(new { tagName });

            return this.View(viewModel);
        }
    }
}