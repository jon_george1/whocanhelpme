﻿namespace WhoCanHelpMe.Presentation.Controllers
{
    using System;
    using System.Net;
    using System.Web.Mvc;

    using MvcContrib;
    using MvcContrib.Filters;

    using WhoCanHelpMe.ApplicationServices.Command;
    using WhoCanHelpMe.ApplicationServices.Contracts;
    using WhoCanHelpMe.Presentation.ActionFilters;
    using WhoCanHelpMe.Presentation.Models;
    using WhoCanHelpMe.Presentation.Queries;

    public class ProfileController : Controller
    {
        private readonly IBus bus;

        private readonly IViewModelQuery<UpdateProfileViewModel> updateProfileViewModelQuery;

        private readonly IViewModelQuery<ViewProfileViewModel> viewProfileViewModelQuery;

        public ProfileController(
            IBus bus, 
            IViewModelQuery<UpdateProfileViewModel> updateProfileViewModelQuery,
            IViewModelQuery<ViewProfileViewModel> viewProfileViewModelQuery)
        {
            this.bus = bus;
            this.updateProfileViewModelQuery = updateProfileViewModelQuery;
            this.viewProfileViewModelQuery = viewProfileViewModelQuery;
        }

        [HttpGet]
        [ModelStateToTempData]
        public ActionResult Create()
        {
            return this.View(new CreateProfileViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModelStateToTempData]
        [Transaction]
        public ActionResult Create(CreateProfileFormModel formModel)
        {
            if (this.ModelState.IsValid)
            {
                var command = new CreateProfileCommand { UserId = this.User.Identity.Name, Name = formModel.Name };

                this.bus.Send(command);

                return this.RedirectToAction(x => x.Update());
            }

            return this.RedirectToAction(x => x.Create());
        }

        [HttpPost]
        [Authorize]
        [Transaction]
        [ModelStateToTempData]
        public ActionResult AddAssertion(AddAssertionFormModel formModel)
        {
            if (this.ModelState.IsValid)
            {
                var command = new AddAssertionCommand
                    {
                        UserId = User.Identity.Name,
                        CategoryId = formModel.CategoryId,
                        Tag = formModel.Tag
                    };

                this.bus.Send(command);
            }

            return this.RedirectToAction(x => x.Update());
        }

        [HttpPost]
        [Authorize]
        [Transaction]
        public ActionResult DeleteAssertion(Guid assertionId)
        {
            if (this.ModelState.IsValid)
            {
                var command = new DeleteAssertionCommand
                    {
                        UserId = User.Identity.Name,
                        AssertionId = assertionId
                    };

                this.bus.Send(command);
            }

            return this.RedirectToAction(x => x.Update());
        }

        [HttpGet]
        [Authorize]
        [Transaction]
        [ModelStateToTempData]
        public ActionResult Update()
        {
            var viewModel = this.updateProfileViewModelQuery.Get(new { UserId = User.Identity.Name });
            
            if (viewModel == null)
            {
                return this.RedirectToAction(x => x.Create());
            }

            return this.View(viewModel);
        }

        public ActionResult View(Guid id)
        {
            var viewModel = this.viewProfileViewModelQuery.Get(new { ProfileId = id });

            if (viewModel == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            return this.View(viewModel);
        }
    }
}