namespace WhoCanHelpMe.Presentation.Controllers
{
    using System.Security.Authentication;
    using System.Web.Mvc;
    using MvcContrib;

    using NHibernate;

    using WhoCanHelpMe.ApplicationServices.Contracts;

    public class UserController : Controller
    {
        private readonly ISecurityService securityService;

        public UserController(ISecurityService securityService)
        {
            this.securityService = securityService;
        }

        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            return this.View();
        }

        [HttpGet]
        public ActionResult Index()
        {
            return this.RedirectToAction(x => x.Login(string.Empty));
        }

        public ActionResult Authenticate(string openId, string returnUrl)
        {
            try
            {
                this.securityService.Authenticate(openId);

                if (!string.IsNullOrEmpty(returnUrl))
                {
                    return this.Redirect(returnUrl);
                }

                return this.RedirectToAction<HomeController>(c => c.IndexAsync());
            }
            catch (AuthenticationException ex)
            {
                this.TempData.Add(
                    "Message",
                    ex.Message);

                return this.RedirectToAction(c => c.Login(returnUrl));
            }
        }

        [HttpGet]
        public ActionResult SignOut()
        {
            this.securityService.SignOut();
            return this.RedirectToAction<HomeController>(x => x.IndexAsync());
        }
    }
}