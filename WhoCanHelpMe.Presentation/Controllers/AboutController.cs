namespace WhoCanHelpMe.Presentation.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using WhoCanHelpMe.Domain.Contracts;

    public class AboutController : AsyncController
    {
        private readonly INewsService newsService;

        public AboutController(INewsService newsService)
        {
            this.newsService = newsService;
        }

        public void IndexAsync()
        {
            AsyncManager.OutstandingOperations.Increment();
            var news = this.newsService.GetDevelopmentTeamBuzzAsync();
            news.ContinueWith(x =>
            {
                AsyncManager.Parameters.Add("headlines", x.Result);
                AsyncManager.OutstandingOperations.Decrement();
            });
        }

        public ActionResult IndexCompleted(IEnumerable<NewsItem> headlines)
        {
            return this.View(headlines.ToList());
        }
    }
}