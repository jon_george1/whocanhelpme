use master
go

if exists(select * from sys.databases where [name] = 'wchm') 
	drop database wchm

create database wchm
go

use wchm
go

create table [CommandLog] (
        Id UNIQUEIDENTIFIER not null,
       UserName NVARCHAR(255) null,
       Data NVARCHAR(255) null,
       CommandName NVARCHAR(max) null,
       ProcessedOn DATETIME null,
       primary key (Id)
    )

create table [Assertion] (
        Id UNIQUEIDENTIFIER not null,
       Version INT not null,
       Tag NVARCHAR(255) null,
       Category_id UNIQUEIDENTIFIER null,
       Profile_id UNIQUEIDENTIFIER null,
       primary key (Id)
    )

    create table [Category] (
        Id UNIQUEIDENTIFIER not null,
       Version INT not null,
       Description NVARCHAR(255) null,
       Name NVARCHAR(255) null,
       primary key (Id)
    )

    create table [Profile] (
        Id UNIQUEIDENTIFIER not null,
       Version INT not null,
       CreatedOn DATETIME null,
       Name NVARCHAR(255) null,
       UserId NVARCHAR(255) null,
       primary key (Id)
    )

    alter table [Assertion] 
        add constraint FKE62653AADDB2D95B 
        foreign key (Category_id) 
        references [Category]

    alter table [Assertion] 
        add constraint FKE62653AA988F7DC5 
        foreign key (Profile_id) 
        references [Profile]

go

INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Has previously worked for', N'Use for listing companies you have worked for', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Is a', N'Use for listing your level / position', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Has worked for', N'Use for listing clients you have worked for', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Is a member of the', N'Use for listing communities you belong to', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Has attended', N'Use for listing conferences you have attended', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Knows about', N'Use for listing topics you know about', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Belongs to the', N'Use for the name of the team you work for', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Is interested in', N'Use for listing topics you are interested in', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Has worked on', N'Use for listing clients you have worked for', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Has training for', N'Use for listing training courses you have attended', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Has experience in', N'Use for listing experiences in business verticals', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Is certified as a', N'Use for listing certifications', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Blogs at', N'Use for your blog Url', newid(), 1)
INSERT [Category] ([Name], [Description], [Id], [Version]) VALUES (N'Tweets as', N'Use for your Twitter handle', newid(), 1)

go